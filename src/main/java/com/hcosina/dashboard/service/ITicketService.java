package com.hcosina.dashboard.service;

import com.hcosina.dashboard.entity.Ticket;

import java.util.List;

public interface ITicketService {
    List<Ticket> getAllTickets();
    Ticket getTicketById(int ticketId);
    List<Ticket> getTicketsByStatus(String status);
    boolean addTicket(Ticket ticket);
    void updateTicket(Ticket ticket);
    void deleteTicket(int ticketId);
}
