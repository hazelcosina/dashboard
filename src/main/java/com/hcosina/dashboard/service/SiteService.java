package com.hcosina.dashboard.service;

import com.hcosina.dashboard.entity.Site;
import com.hcosina.dashboard.repository.ISiteDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SiteService implements ISiteService {
    @Autowired
    private ISiteDao siteDao;

    @Override
    public List<Site> getAllSites() {
        return siteDao.getAllSites();
    }

    @Override
    public Site getSiteById(int articleId) {
        Site obj = siteDao.getSiteById(articleId);
        return obj;
    }

    @Override
    public synchronized boolean addSite(Site site) {
        if (siteDao.siteExists(site.getSiteName())){
            return false;
        } else {
            siteDao.addSite(site);
            return true;
        }
    }

    @Override
    public void updateSite(Site article) {
    }

    @Override
    public void deleteSite(int siteId) {
        siteDao.deleteSite(siteId);
    }
}
