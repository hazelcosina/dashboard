package com.hcosina.dashboard.service;

import com.hcosina.dashboard.entity.Site;
import org.springframework.stereotype.Service;
import java.util.List;

public interface ISiteService {
    List<Site> getAllSites();
    Site getSiteById(int siteId);
    boolean addSite(Site site);
    void updateSite(Site site);
    void deleteSite(int siteId);
}
