package com.hcosina.dashboard.service;

import com.hcosina.dashboard.entity.Scrape;
import com.hcosina.dashboard.entity.Site;
import com.hcosina.dashboard.repository.IScrapeDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ScrapeService implements IScrapeService{

    @Autowired
    private IScrapeDao scrapeDao;

    @Override
    public List<Scrape> getAllScrapes() {
        return scrapeDao.getAllScrapes();
    }

    @Override
    public Scrape getScrapeById(int scrapeId) {
        Scrape obj = scrapeDao.getScrapeById(scrapeId);
        return obj;
    }

    @Override
    public synchronized boolean addScrape(Scrape scrape) {

        if (scrapeDao.scrapeExists(scrape.getFileName())){
            return false;
        } else {
            scrapeDao.addScrape(scrape);
            return true;
        }
    }

    @Override
    public void updateScrape(Scrape scrape) {

    }

    @Override
    public void deleteScrape(int scrapeId) {

    }
}
