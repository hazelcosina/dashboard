package com.hcosina.dashboard.service;

import com.hcosina.dashboard.entity.Scrape;

import java.util.List;

public interface IScrapeService {
    List<Scrape> getAllScrapes();
    Scrape getScrapeById(int scrapeId);
    boolean addScrape(Scrape scrape);
    void updateScrape(Scrape scrape);
    void deleteScrape(int scrapeId);
}
