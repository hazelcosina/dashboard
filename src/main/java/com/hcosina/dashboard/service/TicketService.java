package com.hcosina.dashboard.service;

import com.hcosina.dashboard.entity.Ticket;
import com.hcosina.dashboard.repository.ITicketDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TicketService implements ITicketService {

    @Autowired
    private ITicketDao ticketDao;

    @Override
    public List<Ticket> getAllTickets() {
        return ticketDao.getAllTickets();
    }

    @Override
    public Ticket getTicketById(int ticketId) {
        Ticket obj = ticketDao.getTicketById(ticketId);
        return obj;
    }

    @Override
    public List<Ticket> getTicketsByStatus(String status) {
        return ticketDao.getTicketsByStatus(status);
    }

    @Override
    public boolean addTicket(Ticket ticket) {
        if (ticketDao.ticketExists(ticket.getTicketId())) {
            return false;
        } else {
            ticketDao.addTicket(ticket);
            return true;
        }
    }

    @Override
    public void updateTicket(Ticket ticket) {

    }

    @Override
    public void deleteTicket(int ticketId) {

    }
}
