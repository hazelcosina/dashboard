package com.hcosina.dashboard.http;

import com.hcosina.dashboard.util.DashboardLogger;
import org.jsoup.Connection;
import org.jsoup.Connection.Method;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class HttpFetch {

    private static DashboardLogger log = DashboardLogger.getInstance(HttpFetch.class.getName());
    private HttpCookies cookie = new HttpCookies();
    private HttpParameters parameter = new HttpParameters();

    private String url;
    private String redirectUrl;
    private String stringResult;
    private Document doc;
    private Connection con;
    private int statusCode;

    /**
     * Constructs http fetcher using URL
     *
     * @param url
     */
    public HttpFetch(String url) {
        Connection con = Jsoup
                .connect(url);
        this.con = con;
        this.url = url;
    }

    public HttpFetch(String url, String proxyAddress) {
        setProxy(proxyAddress);
        Connection con = Jsoup
                .connect(url);
        this.con = con;
        this.url = url;
    }

    public HttpFetch(String url, HttpParameters param) {
        Connection con = Jsoup
                .connect(url);

        this.con = con;
        this.parameter = param;
        this.url = url;

    }

    public HttpFetch(String url, HttpCookies cookies) {
        Connection con = Jsoup
                .connect(url);

        this.con = con;
        this.cookie = cookies;
        this.url = url;

    }

    public HttpFetch(String url, HttpParameters param, String proxyAddress) {
        setProxy(proxyAddress);
        Connection con = Jsoup
                .connect(url);

        this.con = con;
        this.parameter = param;
        this.url = url;

    }

    public HttpFetch(String url, HttpCookies cookie, String proxyAddress) {
        setProxy(proxyAddress);
        Connection con = Jsoup
                .connect(url);

        this.con = con;
        this.cookie = cookie;
        this.url = url;

    }

    public HttpFetch(String url, HttpParameters param, HttpCookies cookie) {

        Connection con = Jsoup
                .connect(url);

        this.con = con;
        this.parameter = param;
        this.cookie = cookie;
        this.url = url;

    }

    public HttpFetch(String url, HttpParameters param, HttpCookies cookie, String proxyAddress) {
        setProxy(proxyAddress);
        Connection con = Jsoup
                .connect(url);

        this.con = con;
        this.parameter = param;
        this.cookie = cookie;
        this.url = url;

    }

    public void get() throws IOException, InterruptedException {
        Response res;

        if (parameter.isEmpty() && cookie.isEmpty()) //No parameters and no cookies
        {
            res = con
                    .method(Method.GET)
                    .followRedirects(false)
                    .ignoreHttpErrors(true)
                    .ignoreContentType(true)
                    .execute();
        } else if (!parameter.isEmpty() && cookie.isEmpty()) //Has parameters but no cookies
        {
            res = con
                    .method(Method.GET)
                    .data(parameter.get())
                    .followRedirects(false)
                    .ignoreContentType(true)
                    .ignoreHttpErrors(true)
                    .execute();
        } else if (!cookie.isEmpty() && parameter.isEmpty()) //No parameters but has cookies
        {
            res = con.method(Method.GET)
                    .cookies(cookie.getCookies())
                    .followRedirects(false)
                    .ignoreHttpErrors(true)
                    .ignoreContentType(true)
                    .execute();
        } else { //has both params and cookies
            res = con.method(Method.GET)
                    .data(parameter.get())
                    .cookies(cookie.getCookies())
                    .followRedirects(false)
                    .ignoreHttpErrors(true)
                    .ignoreContentType(true)
                    .execute();
        }

        TimeUnit.SECONDS.sleep(3);

        this.cookie.setCookies(res.cookies());
        this.stringResult = res.body();
        this.doc = res.parse();
        this.statusCode = res.statusCode();
        this.redirectUrl = res.url().toString();

        log.info("\nRequest URL: " + url + " \n"
                + "Cookies : " + res.cookies() + "\n"
                + "StatusCode : " + this.statusCode + "\n"
                + "StatusMessage : " + res.statusMessage() + "\n"
                + "Redirect URL : " + this.redirectUrl + "\n"
        );

    }

    public void post() throws IOException, InterruptedException {
        Response res;

        if (parameter.isEmpty() && cookie.isEmpty()) {
            res = con
                    .method(Method.POST)
                    .followRedirects(false)
                    .ignoreContentType(true)
                    .execute();
        } else if (!parameter.isEmpty() && cookie.isEmpty()) {
            res = con
                    .method(Method.POST)
                    .data(parameter.get())
                    .followRedirects(false)
                    .ignoreContentType(true)
                    .execute();
        } else if (!cookie.isEmpty() && parameter.isEmpty()) {
            res = con.method(Method.POST)
                    .cookies(cookie.getCookies())
                    .followRedirects(false)
                    .ignoreContentType(true)
                    .execute();
        } else {
            res = con.method(Method.POST)
                    .data(parameter.get())
                    .cookies(cookie.getCookies())
                    .followRedirects(true)
                    .ignoreContentType(true)
                    .execute();
        }

        TimeUnit.SECONDS.sleep(3);

        this.cookie.setCookies(res.cookies());
        this.stringResult = res.body();
        this.doc = res.parse();
        this.statusCode = res.statusCode();
        this.redirectUrl = res.url().toString();

        log.info("\nRequest URL: " + url + " \n"
                + "Cookies : " + res.cookies() + "\n"
                + "StatusCode : " + this.statusCode + "\n"
                + "StatusMessage : " + res.statusMessage() + "\n"
                + "Redirect URL : " + this.redirectUrl + "\n"
        );
    }

    public Document getDocument() throws IOException {
        return doc;
    }

    public String getString() {
        return stringResult;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public HttpCookies getCookies() throws IOException {
        return cookie;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    private void setProxy(String proxyAddress) {
        String[] proxyAndPort = proxyAddress.split(":");
        String proxy = proxyAndPort[0];
        String port = proxyAndPort[1];

        System.setProperty("http.proxyHost", proxy);
        System.setProperty("http.proxyPort", port);
    }

    public static Document convertFileToDoc(String file){
        File input = new File(file);
        Document doc = null;
        try {
            doc = Jsoup.parse(input, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return doc;
    }
}
