package com.hcosina.dashboard.http;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author hcosina
 */
public class HttpParameters {

    private Map<String, String> parameters = new HashMap();

    public void set(Map params) {
        this.parameters = params;
    }

    public void add(String name, String value) {
        this.parameters.put(name, value);
    }

    public Map get() {
        return parameters;
    }

    public Boolean isEmpty() {
        Boolean isempty = false;
        if (parameters.isEmpty()) {
            isempty = true;
        }
        return isempty;
    }

}
