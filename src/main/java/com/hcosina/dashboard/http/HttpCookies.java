package com.hcosina.dashboard.http;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author hcosina
 */
public class HttpCookies {

    private Map<String, String> cookies = new HashMap();

    public void setCookies(Map cookies) {
        this.cookies = cookies;
    }

    public void addCookie(String name, String value) {
        this.cookies.put(name, value);
    }

    public Map getCookies() {
        return cookies;
    }
    public String getCookieValue(String key){
        return cookies.get(key);
    }

    /**
     * Returns true if there is no cookie
     * @return
     */
    public Boolean isEmpty() {
        Boolean isempty = false;
        if (cookies.isEmpty()) {
            isempty = true;
        }
        return isempty;
    }
}
