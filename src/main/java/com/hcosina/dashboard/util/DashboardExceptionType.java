package com.hcosina.dashboard.util;

import org.slf4j.MDC;

public enum DashboardExceptionType {

    ERROR_ID("errorID"),
    MISSING_SCRAPE_TICKET_DETAIL("Missing scrape ticket detail."),
    INVALID_SCHEDULE("Failed to create a ticket schedule."),
    INVALID_SCRAPE_SITE("Failed to process ticket. Site is not found in the records.");


    private final String exception;

    private DashboardExceptionType(String constant) {
        this.exception = constant;
    }
    public String value() {
        return exception;
    }

    /**
     * common method to get UI message when there is exception in displaying report, with error id
     *
     * @return String
     */
    public static String getGenericErrorWithID() {
        return "Please contact the Service Desk. Error reference number " + MDC.get(ERROR_ID.value()) + ".";
    }

}

