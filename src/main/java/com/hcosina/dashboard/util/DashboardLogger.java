package com.hcosina.dashboard.util;

import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class DashboardLogger {
    private static final Map<String, org.slf4j.Logger> LOG_MAP = new ConcurrentHashMap<>(0);
    private String className = "";
    private DashboardLogger(String className) {
        this.className = className;
    }
    public static DashboardLogger getInstance(String className) {
        return new DashboardLogger(className);
    }
    public org.slf4j.Logger getLogger(String className) {
        org.slf4j.Logger logger = LOG_MAP.get(className);
        if (logger == null) {
            logger = LoggerFactory.getLogger(className);
            LOG_MAP.put(className, logger);
        }
        return logger;
    }
    public void debug(String message) {
        getLogger(className).debug(message);
    }

    public void debug(String message, Throwable t) {
        getLogger(className).debug(message, t);
    }

    public void debug(String message, Object...object) {
        getLogger(className).debug(message, object);
    }

    public boolean isDebugEnabled(){
        org.slf4j.Logger logger = getLogger(className);
        return logger != null && logger.isDebugEnabled();
    }

    public void info(String message) {
        getLogger(className).info(message);
    }

    public void info(String message, Object...arg) {
        getLogger(className).info(message, arg);
    }

    public void warn(String message) {
        getLogger(className).warn(message);
    }
    public void error(Throwable t) {
        getLogger(className).error(t.getMessage()+" ERROR Id:["+getErrorID()+"]", t);
    }

    public void error(String message, Throwable t) {
        getLogger(className).error(message+" ERROR ID:["+getErrorID()+"]", t);
    }
    public void error(String errMsg) {
        getLogger(className).error("{} ERROR ID:[{}]", errMsg, getErrorID());
    }
    public void trace(String message) {
        getLogger(className).trace(message);
    }
    public void trace(String message, Object...arg) {
        getLogger(className).trace(message, arg);
    }

    public void trace(String operationName, String message, String objectString) {
        getLogger(className).trace("operationName: [ {} ] | {}: [ {} ]", operationName, message, objectString);
    }
    public void perf(String methodName, long responseTime) {
        getLogger(className).info(" PERF_LOG  methodName: [ {} ] EXECUTED IN [ {} ]ms", methodName, responseTime);
    }
    public void perf(String componentName, String operationName, long responseTime) {
        getLogger(className).info(" PERF_LOG  componentName: [ {} ] operationName: [ {} ] EXECUTED IN [ {} ] ms", componentName, operationName ,responseTime);
    }
    private String getErrorID(){
        String errorID = MDC.get("errorID");
        if(errorID==null){
            errorID = UUID.randomUUID().toString();
            MDC.put("errorID", errorID);
        }
        return errorID;
    }

}
