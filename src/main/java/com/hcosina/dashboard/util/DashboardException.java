package com.hcosina.dashboard.util;

public class DashboardException extends Exception {
    
    private static final long serialVersionUID = 1L;
    public DashboardException(Throwable cause) {
        super(cause);
    }
    public DashboardException(String errorMessage) {
        super(errorMessage);
    }
    public DashboardException (DashboardExceptionType errorType, String errorMessage) {
        super(errorType.value() + " - " + errorMessage);
    }

    public DashboardException (DashboardExceptionType errorType, String errorMessage, Throwable cause) {
        super(errorType.value() + " - " + errorMessage, cause);
    }

}
