package com.hcosina.dashboard.util;

import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class FileUtil {

    private static DashboardLogger log = DashboardLogger.getInstance(FileUtil.class.getName());

    public FileUtil(){}

    public String retrieveJsonFileToString(String filePath){
        String st = null;
        JSONParser parser = new JSONParser();

        try {
            Object obj = parser.parse(new FileReader(filePath));

            JSONObject jsonObject = (JSONObject) obj;

            String name = (String) jsonObject.get("Name");
            String author = (String) jsonObject.get("Author");
            JSONArray companyList = (JSONArray) jsonObject.get("Company List");

            System.out.println("Name: " + name);
            System.out.println("Author: " + author);
            System.out.println("\nCompany List:");
            Iterator<String> iterator = companyList.iterator();
            while (iterator.hasNext()) {
                System.out.println(iterator.next());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return st;
    }

    public void exportAsJsonFile(List<Map> data, String fileName){
        JSONObject jsnObj = new JSONObject();
        log.info("exportAsJsonFile -- Filename : "+fileName);
        if(data.size() == 0){
            log.error("Scrape has 0 result");
        }
        int count = 1;
        for (Map<String, String> eachMap : data) {
            jsnObj.put(String.valueOf(count), eachMap);
            log.info("Writing "+eachMap);
            count++;
        }
        log.info(jsnObj.toString());
        try {
            FileWriter fileWriter = new FileWriter(fileName);
            fileWriter.write(jsnObj.toString());
            fileWriter.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
        log.info("Done writing as json");
    }

    public void exportAsXml(ArrayList <Map> data, String fileName){

    }
}
