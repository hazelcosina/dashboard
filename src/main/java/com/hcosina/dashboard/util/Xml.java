package com.hcosina.dashboard.util;

import org.jsoup.Jsoup;
import org.jsoup.select.Elements;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Xml {

    private static DashboardLogger log = DashboardLogger.getInstance(Xml.class.getName());

    /**
     * This exports list results to xml file
     *
     * @param list
     * @param fileName
     * @param fileLocation
     */
    public static void export(List<Map> list, String fileLocation, String fileName, String dateCompleted) {
        try {
            log.info("Writing xml file: " + fileLocation + fileName);

            DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();

            Element root = document.createElement("results");
            document.appendChild(root);
            root.setAttribute("dateCompleted", dateCompleted);

            for (Map eachMap : list) {

                Element result = document.createElement("result");
                root.appendChild(result);

                for (Object key : eachMap.keySet()) {
                    String entry = String.valueOf(eachMap.get(key));
                    Element keyElement = document.createElement(String.valueOf(key));
                    result.appendChild(keyElement);
                    keyElement.appendChild(document.createTextNode(entry));
                }
            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(document);
            StreamResult streamResult = new StreamResult(new File(fileLocation + fileName));

            transformer.transform(domSource, streamResult);

            log.info("Done writing as xml.");
        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        }
    }

    /**
     * This imports xml file to string
     * @param fileLocation
     * @return
     */
    public static List importFromFile(String fileLocation) {
        List elementList = new ArrayList();
        try {
            org.jsoup.nodes.Document doc = null;
            File input = new File(fileLocation);
            doc = Jsoup.parse(input, "UTF-8");

            Elements results = doc.select("result");
            for (org.jsoup.nodes.Element result : results) {

                Map childElements = new HashMap();
                Elements resultChildren = result.children();

                for (org.jsoup.nodes.Element resultChild : resultChildren) {
                    String tagName = resultChild.tagName();
                    childElements.put(tagName,  result.select(tagName).text());
                }

                elementList.add(childElements);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return elementList;
    }
}
