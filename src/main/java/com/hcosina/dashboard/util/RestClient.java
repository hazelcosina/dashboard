package com.hcosina.dashboard.util;

import com.hcosina.dashboard.entity.Scrape;

import java.net.URI;

import com.hcosina.dashboard.entity.Ticket;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

public class RestClient {
    private static final DashboardLogger log = DashboardLogger.getInstance(RestClient.class.getName());

    public void addScrape(Scrape scrape) {
        log.info("Saving scrape to db");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/addScrape";
        HttpEntity<Scrape> requestEntity = new HttpEntity<Scrape>(scrape, headers);
        URI uri = restTemplate.postForLocation(url, requestEntity);

        log.info(uri.getPath());
    }

    public void addTicket(Ticket ticket){
        log.info("Saving ticket to db");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/addTicket";
        HttpEntity<Ticket> requestEntity = new HttpEntity<Ticket>(ticket, headers);
        URI uri = restTemplate.postForLocation(url, requestEntity);

        String path = uri.getPath();
        int ticketId = Integer.parseInt(path.substring(path.lastIndexOf("/")+1));
        ticket.setTicketId(ticketId);
    }
}
