package com.hcosina.dashboard;

import com.hcosina.dashboard.util.Xml;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;

import java.util.List;
import java.util.Map;

@SpringBootApplication
@EnableAutoConfiguration
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);

	}
}