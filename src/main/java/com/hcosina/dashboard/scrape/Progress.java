package com.hcosina.dashboard.scrape;

/**
 * @author hazel.cosina
 */
public class Progress {

    private String siteName;
    private int ticketId;
    private int scrapeId;
    private String scrapeStatus;
    private String ticketStatus;
    private String remarks;
    private String type;
    private int targetCount;
    private int currentCount;

    public Progress(){}

    public void setSiteName(String siteName){ this.siteName = siteName; }

    public String getSiteName(){ return siteName; }

    public void setTicketId(int ticketId){ this.ticketId = ticketId; }

    public int getTicketId(){ return ticketId; }

    public void setScrapeId(int scrapeId){ this.scrapeId = scrapeId; }

    public int getScrapeId(){ return scrapeId; }

    public void setScrapeStatus(String status){ this.scrapeStatus = status; }

    public String getScrapeStatus() { return scrapeStatus; }

    public void setTicketStatus(String status) { this.ticketStatus = status; }

    public String getTicketStatus(){ return ticketStatus; }

    public void setRemarks(String remarks){ this.remarks = remarks;}

    public String getRemarks(){ return remarks;}

    public void setTargetCount(int targetCount){ this.targetCount = targetCount;}

    public int getTargetCount(){ return targetCount;}

    public void setCurrentCount(int currentCount){ this.currentCount = currentCount;}

    public int getCurrentCount(){ return currentCount;}

    public void setType(String type) {  this.type = type;  }

    public String getType() { return type; }

}
