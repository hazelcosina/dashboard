package com.hcosina.dashboard.scrape;

import com.hcosina.dashboard.entity.Scrape;
import com.hcosina.dashboard.entity.Ticket;
import com.hcosina.dashboard.util.*;

import java.sql.Date;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class ScrapeHandler {

    private static final DashboardLogger log = DashboardLogger.getInstance(ScrapeHandler.class.getName());
    private static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd/ HH:mm:ss");

    protected Scrape scrape;
    protected List results;

    public Scrape initialize(Ticket ticket) {
        log.info("Starting to scrape from ticket: "+ticket.toString());

        scrape = new Scrape();
        scrape.setTicket(ticket);
        scrape.setStatus("Running");
        scrape.setRemarks("Started at " + dtf.format(LocalDateTime.now()));
        scrape.setDateStarted(LocalDateTime.now().toString());
        scrape.generateOtherDetails();
        return scrape;
    }

    public void updateCurrentCount(int count) {
        scrape.setRemarks("Saving item " + count + " of " + count);
        scrape.setCurrentCount(count);
    }

    public void save(ArrayList list) {
        Ticket ticket = scrape.getTicket();
        log.info("Shutting down " + scrape.getScrapeName());

        if (scrape.getCurrentCount() >= scrape.getTargetCount()) {
            scrape.setRemarks("Scrape is completed.");
        } else {
            scrape.setRemarks("Scrape is completed but target is not reached."); //Completed with warnings
        }
        log.info("List size: " + list.size());

        if (!ticket.getIsOnRepeat()) {
            ticket.setStatus("Closed");
            scrape.setStatus("Closed");
            ticket.setDateClosed(Date.valueOf(LocalDateTime.now().toLocalDate()));
            scrape.setDateCompleted(String.valueOf(LocalDateTime.now().toLocalDate()));
        } else {
            scrape.setStatus("Closed");
            ticket.setStatus("Active");
        }
        scrape.setDateCompleted(String.valueOf(LocalDateTime.now().toLocalDate()));

        setResults(list);
        export(ticket);

        RestClient restUtil = new RestClient();
        restUtil.addScrape(scrape);
    }

    public void export(Ticket ticket) {
        String fileType = ticket.getFileExport();
        String fileName = scrape.getFileName();
        String fileLocation = scrape.getFileLocation();
        String dateCompleted = scrape.getDateCompleted();

        log.info("Exporting to " + fileName);
        switch (fileType) {
            case "xls":
//                new Xls(data, fileName + ".xls").create();
//                break;
            case "json":
                FileUtil a = new FileUtil();
                a.exportAsJsonFile(results, fileLocation + fileName);
                break;
//            case "csv":
//                new Csv(data, fileName + ".csv").create();
//                break;
            case "xml":
                Xml.export(results, fileLocation, fileName, dateCompleted);
                break;
            default:
                break;
        }
    }

    public List getResults() {
        return results;
    }

    public void setResults(List results) {
        this.results = results;
    }
}
