package com.hcosina.dashboard.scrape;

import com.hcosina.dashboard.entity.Scrape;
import com.hcosina.dashboard.entity.Ticket;
import com.hcosina.dashboard.sites.CraigList;
import com.hcosina.dashboard.sites.Ebay;
import com.hcosina.dashboard.util.DashboardLogger;
import com.hcosina.dashboard.util.RestClient;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.Callable;

/**
 * @author hcosina
 */

public class ScrapeThread implements Callable<Scrape> {

    private static DashboardLogger log = DashboardLogger.getInstance(ScrapeThread.class.getName());

    private Scrape scrape;
    private Ticket ticket;

    public ScrapeThread(Ticket ticket) {
        this.ticket = ticket;
    }

    public Scrape call() throws Exception {

        RestClient rest = new RestClient();
        rest.addTicket(ticket);

        log.info("ticketID: "+ticket.getTicketId());

        Thread.sleep(1000);
        JobDataMap map = new JobDataMap();

        log.info("Site: " + ticket.getSiteName());

        switch (ticket.getSiteName()) {
            case "Craiglist":
                if (ticket.getAction().equals("on_demand")) {
                    log.info("Processing on demand scrape for Craiglist.");
                    scrape = CraigList.getInstance().doScrape(ticket);
                } else {
                    log.info("Processing a schedule for craiglist");
                    log.info("Cron is: " + ticket.getSchedule());

                    map.put("request", ticket);

                    JobDetail job = JobBuilder.newJob(CraigList.class)
                            .withIdentity(ticket.getSiteName() + "_" + ticket.getTicketNumber())
                            .usingJobData(map).build();
                    //scrapeClone.setRemarks("In queue");
                    createSchedule(job, ticket.getSchedule());
                }
                break;
            case "Ebay":
                if (ticket.getAction().equals("on_demand")) {
                    log.info("Processing on demand scrape for Ebay.");
                   // scrapeClone = Ebay.getInstance().doScrape(ticket);
                } else {
                    log.info("Setting a schedule for Ebay");
                    log.info("Cron is: " + ticket.getSchedule());

                    map.put("request", ticket);

                    JobDetail job = JobBuilder.newJob(Ebay.class)
                            .withIdentity(ticket.getSiteName() + "_" + ticket.getTicketNumber())
                            .usingJobData(map).build();
                    //scrapeClone.setRemarks("In queue");
                    createSchedule(job, ticket.getSchedule());
                }
                break;
            default:
                throw new Exception("Site not found.");
        }
        return scrape;
    }

    public void createSchedule(JobDetail job, String cron) {
        try {
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
            String yyyyMMddHHmmss = dtf.format(LocalDateTime.now());


            Trigger trigger = TriggerBuilder
                    .newTrigger()
                    .withIdentity(ticket.getSiteName(), yyyyMMddHHmmss)
                    .withSchedule(
                            CronScheduleBuilder.cronSchedule(cron))
                    .build();

          //  scrapeClone.setJobTrigger(ticket.getSiteName()+"_"+ ticket.getTicketNumber()+"_"+yyyyMMddHHmmss);

            Scheduler scheduler = new StdSchedulerFactory().getScheduler();
            scheduler.start();
            scheduler.scheduleJob(job, trigger);
        } catch (SchedulerException e) {
            ticket.setStatus("Failed.");
            e.printStackTrace();
        }
    }
}
