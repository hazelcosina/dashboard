package com.hcosina.dashboard.entity;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SiteRowMapper implements RowMapper<Site> {

    @Override
    public Site mapRow(ResultSet row, int rowNum) throws SQLException {
        Site site = new Site();
        site.setSiteId(row.getInt("siteId"));
        site.setSiteName(row.getString("siteName"));
        return site;
    }
}
