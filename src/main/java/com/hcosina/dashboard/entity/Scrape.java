package com.hcosina.dashboard.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Scrape {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "scrapeId", updatable = false, nullable = false)
    private int scrapeId;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ticketId")
    @JsonBackReference
    private Ticket ticket;

    private String scrapeName;
    private String fileName;
    private String fileLocation;
    private int targetCount;
    private int currentCount;
    private String dateCompleted;
    private String dateStarted;
    private String status;
    private String remarks;
    private String jobTrigger;

    public Scrape() {
    }

    public int getScrapeId() {
        return scrapeId;
    }

    public void setScrapeId(int id) {
        this.scrapeId = id;
    }

    public void setScrapeName(String scrapeName) {
        this.scrapeName = scrapeName;
    }

    public String getScrapeName() {
        return this.scrapeName;
    }

    public void setFileName(String filename) {
        this.fileName = filename;
    }

    public String getFileName() {
        return this.fileName;
    }

    public void setFileLocation(String fileLocation) {
        this.fileLocation = fileLocation;
    }

    public String getFileLocation() {
        return fileLocation;
    }

    public void setTargetCount(int targetCount) {
        this.targetCount = targetCount;
    }

    public int getTargetCount() {
        return targetCount;
    }

    public void setCurrentCount(int currentCount) {
        this.currentCount = currentCount;
    }

    public int getCurrentCount() {
        return currentCount;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setJobTrigger(String jobTrigger) {
        this.jobTrigger = jobTrigger;
    }

    public String getJobTrigger() {
        return jobTrigger;
    }

    public void setDateCompleted(String dateCompleted) {
        this.dateCompleted = dateCompleted;
    }

    public String getDateCompleted() {
        return dateCompleted;
    }

    public void setDateStarted(String date) {
        this.dateStarted = dateStarted;
    }

    public String getDateStarted() {
        return dateStarted;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    public Ticket getTicket() {
        return ticket;
    }

    public void generateOtherDetails(){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        DateTimeFormatter htf = DateTimeFormatter.ofPattern("HHmmss");

        String yyyyMMddHHmmss = dtf.format(LocalDateTime.now());
        String hhmmss = htf.format(LocalDateTime.now());

        setScrapeName(ticket.getSiteName());
        setScrapeId(ticket.getTicketId()+Integer.parseInt(hhmmss));
        setFileLocation("C:\\Projects\\TestFiles\\Results\\");
        setFileName(ticket.getSiteName() + "_" + ticket.getType() + "_" + ticket.getTicketNumber() + "_" + yyyyMMddHHmmss +"."+ ticket.getFileExport());
    }

    @Override
    public String toString() {
        return "Scrape{" +
                "scrapeId=" + scrapeId +
                ", ticket=" + ticket +
                ", scrapeName='" + scrapeName + '\'' +
                ", fileName='" + fileName + '\'' +
                ", fileLocation='" + fileLocation + '\'' +
                ", targetCount=" + targetCount +
                ", currentCount=" + currentCount +
                ", dateCompleted='" + dateCompleted + '\'' +
                ", dateStarted='" + dateStarted + '\'' +
                ", status='" + status + '\'' +
                ", remarks='" + remarks + '\'' +
                ", jobTrigger='" + jobTrigger + '\'' +
                '}';
    }
}