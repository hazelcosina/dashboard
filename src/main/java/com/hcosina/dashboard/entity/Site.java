package com.hcosina.dashboard.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Site {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int siteId;
    private String siteName;

    public void setSiteId(int siteId){ this.siteId =siteId;}
    public int getSiteId() { return siteId;}
    public void setSiteName(String siteName){ this.siteName = siteName;}
    public String getSiteName(){ return siteName;}
}
