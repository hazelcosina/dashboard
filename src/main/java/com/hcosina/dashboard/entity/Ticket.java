package com.hcosina.dashboard.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.sql.Date;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Entity
@Table(name = "ticket")
public class Ticket {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ticketId", updatable = false, nullable = false)
    private int ticketId;

    @OneToMany(targetEntity = Scrape.class, mappedBy = "ticket", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<Scrape> scrape = new ArrayList<>();

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "userId")
    private User user;

    private int ticketNumber;
    private int siteId;
    private String keyword;
    private Date dateCreated;
    private Date dateClosed;
    private String status;
    private String schedule;
    private String action;
    private String fileExport;
    private Boolean hasImage;
    private String proxyAddress;
    private String userName;
    private String password;
    private Boolean isOnRepeat;
    private String siteName;
    private String type; //info or image

    public Ticket() {
    }

    public void setScrape(List<Scrape> scrape) {
        this.scrape = scrape;
    }

    public List<Scrape> getScrape() {
        return scrape;
    }

    public void addScrape(Scrape scrape){ this.scrape.add(scrape); }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setTicketId(int ticketId) {
        this.ticketId = ticketId;
    }

    public int getTicketId() {
        return ticketId;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setSiteId(int siteId) {
        this.siteId = siteId;
    }

    public int getSiteId() {
        return siteId;
    }

    public void setTicketNumber(int ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public int getTicketNumber() {
        return ticketNumber;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateClosed(Date dateClosed) {
        this.dateClosed = dateClosed;
    }

    public Date getDateClosed() {
        return dateClosed;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getAction() {
        return action;
    }

    public void setFileExport(String fileExport) {
        this.fileExport = fileExport;
    }

    public String getFileExport() {
        return fileExport;
    }

    public void setHasImage(Boolean hasImage) {
        this.hasImage = hasImage;
    }

    public Boolean getHasImage() {
        return hasImage;
    }

    public void setProxyAddress(String proxyAddress) {
        this.proxyAddress = proxyAddress;
    }

    public String getProxyAddress() {
        return proxyAddress;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public void setPassword(String pass) {
        this.password = pass;
    }

    public String getPassword() {
        return password;
    }

    public void setIsOnRepeat(Boolean isOnRepeat) {
        this.isOnRepeat = isOnRepeat;
    }

    public Boolean getIsOnRepeat() {
        return this.isOnRepeat;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setType(String type) {  this.type = type;  }

    public String getType() { return type; }

    public void generateOtherDetails() {
        //String dateToday = Date.valueOf(LocalDateTime.now().toString());

        String ticketNumber = String.valueOf(new Random().nextInt(999999));
        setTicketNumber(Integer.parseInt(ticketNumber));

        //Assigning has image
        if (getHasImage() == false) {
            setType("Info");
        } else {
            setType("Image");
        }

        //Creating schedule
        String[] scheduleArr = getSchedule().split(",");

        if (getAction().equals("radio-repeat")) {
            setIsOnRepeat(true);
            String day = scheduleArr[0];    //for onRepeat schedule
            String hour = scheduleArr[1];   //for onRepeat schedule
            String cronSchedule = "0 0/3 " + hour + " ? * " + day;
            setSchedule(cronSchedule);
            setAction("on_repeat");

        } else if (getAction().equals("radio-setdate")) {
            setIsOnRepeat(false);
            String[] date = scheduleArr[2].split("/");   //for one time schedule
            String dateTime = scheduleArr[3];   //for one time schedule
            String mm = date[0];
            String dd = date[1];
            String yyyy = date[2];
            String cronDate = "0 0 " + dateTime + " " + dd + " " + mm + " ? " + yyyy;
            setSchedule(cronDate);
            setAction("scheduled_date");
        } else {
            setIsOnRepeat(false);
            DateTimeFormatter dtfForSchedule = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm");
            String currentDateAndTime = dtfForSchedule.format(LocalDateTime.now());
            setSchedule(currentDateAndTime);
            setAction("on_demand");
        }

    }

    @Override
    public String toString() {
        return "Ticket{" +
                "ticketId=" + ticketId +
                ", scrape=" + scrape +
                ", user=" + user +
                ", ticketNumber=" + ticketNumber +
                ", siteId=" + siteId +
                ", keyword='" + keyword + '\'' +
                ", dateCreated=" + dateCreated +
                ", dateClosed=" + dateClosed +
                ", status='" + status + '\'' +
                ", schedule='" + schedule + '\'' +
                ", action='" + action + '\'' +
                ", fileExport='" + fileExport + '\'' +
                ", hasImage=" + hasImage +
                ", proxyAddress='" + proxyAddress + '\'' +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", isOnRepeat=" + isOnRepeat +
                ", siteName='" + siteName + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
