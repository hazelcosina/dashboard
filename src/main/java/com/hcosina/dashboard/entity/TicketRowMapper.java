package com.hcosina.dashboard.entity;

import org.springframework.jdbc.core.RowMapper;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

public class TicketRowMapper implements RowMapper<Ticket> {
    @Override
    public Ticket mapRow(ResultSet resultSet, int i) throws SQLException {
        Ticket ticket = new Ticket();
        ticket.setTicketId(resultSet.getInt("ticket_id"));
        ticket.setSiteId(resultSet.getInt("site_id"));
        ticket.setSiteName(resultSet.getString("site_name"));
        ticket.setKeyword(resultSet.getString("keyword"));
        ticket.setProxyAddress(resultSet.getString("proxy_address"));
        ticket.setHasImage(resultSet.getBoolean("has_image"));
        ticket.setAction(resultSet.getString("action"));
        ticket.setSchedule(resultSet.getString("schedule"));
        ticket.setFileExport(resultSet.getString("file_export"));
        ticket.setStatus(resultSet.getString("status"));
        ticket.setUserName(resultSet.getString("user_name"));
        ticket.setPassword(resultSet.getString("password"));
        ticket.setDateCreated(resultSet.getDate("date_created"));
        ticket.setTicketNumber(resultSet.getInt("ticket_number"));
        ticket.setDateClosed(resultSet.getDate("date_closed"));
        ticket.setIsOnRepeat(resultSet.getBoolean("is_on_repeat"));
        ticket.setType(resultSet.getString("type"));
        return ticket;
    }
}
