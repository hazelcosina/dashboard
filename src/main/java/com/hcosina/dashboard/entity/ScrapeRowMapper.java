package com.hcosina.dashboard.entity;

import org.springframework.jdbc.core.RowMapper;

import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ScrapeRowMapper implements RowMapper<Scrape> {
    @Override
    public Scrape mapRow(ResultSet row, int i) throws SQLException {
        Scrape scrape = new Scrape();
        scrape.setScrapeId(row.getInt("scrape_id"));
        scrape.setScrapeName(row.getString("scrape_name"));
        scrape.setFileName(row.getString("file_name"));
        scrape.setFileLocation(row.getString("file_location"));
        scrape.setTargetCount(row.getInt("target_count"));
        scrape.setCurrentCount(row.getInt("current_count"));
        scrape.setDateCompleted(row.getString("date_completed"));
        scrape.setDateStarted(row.getString("date_started"));
        scrape.setStatus(row.getString("status"));
        scrape.setRemarks(row.getString("remarks"));
        scrape.setJobTrigger(row.getString("job_trigger"));
        return scrape;
    }
}
