package com.hcosina.dashboard.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int userId;

    @OneToMany(targetEntity= Ticket.class, mappedBy = "user",fetch=FetchType.EAGER, cascade = CascadeType.ALL)
    private List<Ticket> ticketEntities;

    private String firstName;
    private String lastName;
    private String userRole;

    public User(){}
    public User(String firstName, String lastName, String userRole){
        this.firstName = firstName;
        this.lastName = lastName;
        this.userRole = userRole;
    }

    public void setUserId(int userId){ this.userId = userId;}
    public int getUserId(){ return userId;}
    public void setFirstName(String firstName){ this.firstName = firstName;}
    public String getFirstName(){ return firstName;}
    public void setLastName(String lastName){ this.lastName = lastName;}
    public String getLastName(){ return lastName;}
    public void setUserRole(String userRole){ this.userRole = userRole;}
    public String getUserRole(){ return userRole;}
}
