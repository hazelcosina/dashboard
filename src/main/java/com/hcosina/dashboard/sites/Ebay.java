package com.hcosina.dashboard.sites;

import com.hcosina.dashboard.entity.Scrape;
import com.hcosina.dashboard.entity.Ticket;
import com.hcosina.dashboard.http.HttpCookies;
import com.hcosina.dashboard.http.HttpFetch;
import com.hcosina.dashboard.http.HttpParameters;
import com.hcosina.dashboard.scrape.ScrapeHandler;
import com.hcosina.dashboard.util.DashboardLogger;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public class Ebay extends ScrapeHandler implements Job {
    private static DashboardLogger log = DashboardLogger.getInstance(Ebay.class.getName());
    private static final Ebay instance = new Ebay();
    private HttpFetch fetcher;
    private HttpParameters param;
    private HttpCookies cookies;

    public static Ebay getInstance(){
        return instance;
    }
    public Scrape doScrape(Ticket ticket){
        log.info("Starting to scrape ebay");

        initialize(ticket);
        try {
            Document doc = null;
            String keyword = "iphone";

//            cookies = new HttpCookies();
//            fetcher = new HttpFetch("https://www.ebay.ph/");
//            fetcher.get();
//
//            cookies = fetcher.getCookies();
//
//            param = new HttpParameters();
//            param.add("_okdw",keyword);
//            param.add("_osacat","0");
//            param.add("_from","R40");
//            param.add("_trksid","m570.l1313");
//            param.add("_nkw",keyword);
//            param.add("_sacat","0");
//
//            fetcher = new HttpFetch("https://www.ebay.ph/sch/i.html", param, cookies);
//            fetcher.get();
//
//            doc = fetcher.getDocument();

            doc = fetcher.convertFileToDoc("C:\\Projects\\TestFiles\\ebay_iphone.html");

            int searchResults = Integer.parseInt(doc.select(".rcnt").text());
            scrape.setTargetCount(searchResults);

            Element list = doc.select("ul#ListViewInner").first();
            Elements listItems = list.select("li");

            int x=1;
            ArrayList dataList = new ArrayList();
            LinkedHashMap<String, Object> data = null;

            for(Element item : listItems){
                String id = String.valueOf(x);
                String title = item.select(".lvtitle a").text();
                String listingId = item.attr(("listingid"));
                String price = item.select("span.bold").text();
                String productLink = item.select(".imgWr2").attr("href");
                String imageLink = item.select("img.img").attr("src");

                if(!listingId.isEmpty() && !title.isEmpty()){
                    data = new LinkedHashMap();
                    data.put("id", id);
                    data.put("title",title);
                    data.put("listing", listingId);
                    data.put("price", price);
                    data.put("product", "<a href=\""+productLink+"\">Link</a>");
                    data.put("images", imageLink);

                    dataList.add(data);


                    updateCurrentCount(x);
                    x++;
                }
            }

            save(dataList);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return scrape;
    }
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {

    }
}
