package com.hcosina.dashboard.sites;

import com.hcosina.dashboard.entity.Craigslist;
import com.hcosina.dashboard.entity.Scrape;
import com.hcosina.dashboard.entity.Ticket;
import com.hcosina.dashboard.scrape.ScrapeHandler;
import com.hcosina.dashboard.util.DashboardLogger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author hcosina
 */
public class CraigList extends ScrapeHandler implements Job {

    private static final CraigList instance = new CraigList();
    private static DashboardLogger log = DashboardLogger.getInstance(Craigslist.class.getName());

    private ArrayList<Map> list = new ArrayList();
    private String keyword;
    private Boolean hasNextPage;
    private WebDriver driver;
    private int count;

    public static CraigList getInstance() {
        return instance;
    }

    public Scrape doScrape(Ticket ticket) {
        scrape = initialize(ticket);
        ticket.addScrape(scrape);

        keyword = ticket.getKeyword();

        hasNextPage = true;

        driver = new HtmlUnitDriver();
        driver.get("https://sfbay.craigslist.org/");

        WebElement textboxSearch = driver.findElement(By.id("query"));
        textboxSearch.sendKeys(keyword);
        textboxSearch.submit();

        //Getting target count
        WebElement textTotalCount = driver.findElement(By.xpath("//span[@class='totalcount'][1]"));
        String totalCount = textTotalCount.getText();
     //   scrape.setTargetCount(Integer.parseInt(totalCount));
        scrape.setTargetCount(Integer.parseInt(totalCount));

        log.info("Total Count: " + totalCount);

        count = 0;

        //Loop search results pages while next page button ic clickable
        try{
        do {

            //Locating nextpage button
            WebElement nextPageButton = driver.findElement(By.xpath("//a[@class='button next']"));
            String pageNumberText = driver.findElement(By.xpath("//span[@class='button pagenum']")).getText();
            log.info("Page: " + pageNumberText);

            scrape.setRemarks("Now in page " + pageNumberText);

            Document doc = Jsoup.parse(driver.getPageSource());
            parseCurrentPage(doc);

            scrape.setCurrentCount(count);

            String nextPageLink = doc.select(".next").get(0).attr("href");
            if (nextPageLink == "") {
                hasNextPage = false;
            } else {
                nextPageButton.click();
            }
        } while (hasNextPage);
        }catch(Exception e){
log.error(e.getMessage());
        }

        save(list);
        driver.quit();
        return scrape;
    }

    public void parseCurrentPage(Document doc) {
        try {
            Elements resultRow = doc.select("li.result-row");
            for (Element result : resultRow) {
                count++;
                LinkedHashMap<String, Object> data = new LinkedHashMap();

                String title = result.select(".result-title").text();
                String price = result.select(".result-price").text();

                //Some items have 2 prices displayed. Selecting only 1
                String[] twoPrice = price.split(" ");
                if (twoPrice.length > 1) {
                    price = twoPrice[0];
                }

                String date = result.select(".result-date").text();
                String location = result.select(".result-hood").text();
                String pid = result.attr("data-pid");
                String href = result.select(".result-image").attr("data-ids");

                data.put("ID", String.valueOf(count));
                data.put("Title", title);
                data.put("Price", price);
                data.put("Date", date);
                data.put("Location", location);
                data.put("PID", pid);

                ArrayList urls = new ArrayList();

                if (href != null) {
                    String[] byComma = href.split(",");
                    for (String eachComma : byComma) {
                        try {
                            String BASE_URL = "https://images.craigslist.org/";
                            String ext = "_600x450.jpg";
                            String[] eachColon = eachComma.split(":");

                            String imgName = eachColon[1] + ext;
                            String url = BASE_URL + imgName;
                            urls.add(url);
                        } catch (ArrayIndexOutOfBoundsException e) {
                            System.out.println("No image for: " + title);
                        }
                    }

                }

                //        TimeUnit.SECONDS.sleep(2);

                data.put("Images", urls);
                list.add(data);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) {

        log.info("Job for craiglist is starting..");
        try {

            JobDataMap dataMap = jobExecutionContext.getJobDetail().getJobDataMap();
            Ticket ticket = (Ticket) dataMap.get("request");
           // Progress progress = (Progress) dataMap.get("progress");
            doScrape(ticket);

            log.info("Job for craiglist is done.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
