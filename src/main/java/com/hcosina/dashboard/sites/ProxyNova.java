package com.hcosina.dashboard.sites;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author hcosina This gets proxy from https://free-proxy-list.net/
 */
public class ProxyNova {

    private static final String IPADDRESS_PATTERN
            = "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
            + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
            + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\."
            + "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";

    public ArrayList getAvailableProxies(String location, String anon) throws IOException, InterruptedException {

//        HttpFetch fetcher = new HttpFetch("https://www.proxynova.com/proxy-server-list/");
//        fetcher.get();
        File input = new File("C:\\Projects\\TestFiles\\proxynova.html");
        Document doc = Jsoup.parse(input, "UTF-8");

        // Document doc = fetcher.getDocument();
        ArrayList<Map> list = new ArrayList();

        Elements proxyElems = doc.select("tr");
        for (Element proxyElem : proxyElems) {
            if (proxyElem.hasAttr("data-proxy-id")) {
                String ip = proxyElem.select("abbr").attr("title");

                if (ip.matches(IPADDRESS_PATTERN)) {
                    Map proxy = filter(ip, location, anon, proxyElem);
                    if (!proxy.isEmpty()) {

                        list.add(proxy);
                    }
                }

            }

        }
        return list;
    }

    private Map<String, String> filter(String ip, String location, String anon, Element proxyElem) {
        Map proxy = new HashMap();

        String loc = proxyElem.select("td").get(5).text();
        String port = proxyElem.select("td").get(1).text();
        String lastCheck = proxyElem.select("td").get(2).text();
        String anonimity = proxyElem.select("td").get(6).text();
        String speed = proxyElem.select("td").get(3).text();

        if (!location.equalsIgnoreCase("All") || !anon.equalsIgnoreCase("All")) {
            if (loc.contains(location) && anon.equalsIgnoreCase("All")) {
                proxy.put("location", loc);
                proxy.put("proxyaddress", ip);
                proxy.put("port", port);
                proxy.put("source", "https://www.proxynova.com/");
                proxy.put("lastcheck", lastCheck);
                proxy.put("anonimity", anonimity);
                proxy.put("speed", speed);

            }

            if (location.equalsIgnoreCase("All") && anonimity.contains(anon)) {
                proxy.put("location", loc);
                proxy.put("proxyaddress", ip);
                proxy.put("port", port);
                proxy.put("source", "https://www.proxynova.com/");
                proxy.put("lastcheck", lastCheck);
                proxy.put("anonimity", anonimity);
                proxy.put("speed", speed);

            }

            if (loc.contains(location) && anonimity.contains(anon)) {
                proxy.put("location", loc);
                proxy.put("proxyaddress", ip);
                proxy.put("port", port);
                proxy.put("source", "https://www.proxynova.com/");
                proxy.put("lastcheck", lastCheck);
                proxy.put("anonimity", anonimity);
                proxy.put("speed", speed);

            }

        } else {
            proxy.put("location", loc);
            proxy.put("proxyaddress", ip);
            proxy.put("port", port);
            proxy.put("source", "https://www.proxynova.com/");
            proxy.put("lastcheck", lastCheck);
            proxy.put("anonimity", anonimity);
            proxy.put("speed", speed);
        }

        return proxy;
    }

}
