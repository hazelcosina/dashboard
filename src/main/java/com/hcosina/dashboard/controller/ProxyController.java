/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hcosina.dashboard.controller;

import com.hcosina.dashboard.sites.ProxyNova;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author hcosina
 */
@Controller
public class ProxyController {

    @GetMapping("/proxy")
    public String records() {
        return "proxy";
    }

    @GetMapping(value = "/retrieveproxy")
    public @ResponseBody ArrayList getAllProxies(@RequestParam String location, @RequestParam String anonimity) throws IOException, InterruptedException {
        return new ProxyNova().getAvailableProxies(location, anonimity);
    }
}
