/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hcosina.dashboard.controller;

import com.hcosina.dashboard.entity.Scrape;
import com.hcosina.dashboard.service.IScrapeService;
import com.hcosina.dashboard.util.Xml;
import com.hcosina.dashboard.util.DashboardLogger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * @author hcosina
 */
@Controller
public class RecordsController {

    private static DashboardLogger log = DashboardLogger.getInstance(RecordsController.class.getName());

    @Autowired
    private IScrapeService scrapeService;

    @GetMapping("/records")
    public String records(@RequestParam(name = "name", required = false, defaultValue = "World") String name, Model model) {
        model.addAttribute("name", name);
        return "records";
    }

    @GetMapping("/view")
    public String record(@RequestParam(name = "scrapeid", required = true) String requestId, Model model) {
        model.addAttribute("requestid", requestId);
        return "view";
    }

    @GetMapping(path = "/getAllRecords")
    public ResponseEntity<List<Scrape>> getAllRecords() {
        List<Scrape> list = scrapeService.getAllScrapes();
        return new ResponseEntity<List<Scrape>>(list, HttpStatus.OK);
    }


    @GetMapping(path = "/getScrapeDetail")
    public @ResponseBody
    List getScrapeDetail(String scrapeId) {
        Scrape scrape = scrapeService.getScrapeById(Integer.parseInt(scrapeId));
        String file = scrape.getFileLocation() + scrape.getFileName();

        List scrapeRecord = new ArrayList();
        scrapeRecord = Xml.importFromFile(file);

        return scrapeRecord;
    }

    @GetMapping(path = "/download")
    public @ResponseBody
    String download(HttpServletRequest request) {
        String[] editValues = request.getParameterValues("newValueArr");
        for (String newValue : editValues) {
            System.out.println(newValue);
        }
        return "downloaded";
    }

    @PostMapping(path = "/save")
    public @ResponseBody
    String saveChanges(HttpServletRequest request) {

        String editedValues = request.getParameter("newvalues");
        System.out.println(editedValues);
        System.out.println(JSONObject.valueToString(editedValues));

//
//
//        for (String newValue : editedValues) {
//            System.out.println(newValue);
//        }
        return "saved";
    }
}
