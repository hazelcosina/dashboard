package com.hcosina.dashboard.controller;

import com.hcosina.dashboard.entity.Site;
import com.hcosina.dashboard.service.ISiteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;

@Controller
@RequestMapping("user")
public class SiteController {
    @Autowired
    private ISiteService siteService;
    @GetMapping("site/{id}")
    public ResponseEntity<Site> getSiteById(@PathVariable("id") Integer id) {
        Site article = siteService.getSiteById(id);
        return new ResponseEntity<Site>(article, HttpStatus.OK);
    }

    @GetMapping("site")
    public ResponseEntity<List<Site>> getAllSites() {
        List<Site> list = siteService.getAllSites();
        return new ResponseEntity<List<Site>>(list, HttpStatus.OK);
    }

    @PostMapping("site")
    public ResponseEntity<Void> addSite(@RequestBody Site site, UriComponentsBuilder builder) {
        boolean flag = siteService.addSite(site);
        if (flag == false) {
            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(builder.path("/article/{id}").buildAndExpand(site.getSiteId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }


}
