package com.hcosina.dashboard.controller;

import com.hcosina.dashboard.entity.*;
import com.hcosina.dashboard.scrape.Progress;
import com.hcosina.dashboard.scrape.ScrapeThread;
import com.hcosina.dashboard.service.IScrapeService;
import com.hcosina.dashboard.service.ITicketService;
import com.hcosina.dashboard.util.DashboardLogger;
import com.hcosina.dashboard.util.RestClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.sql.Date;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * @author hazel.cosina
 */
@Controller
public class ScrapeController {

    private static DashboardLogger log = DashboardLogger.getInstance(ScrapeController.class.getName());

    private List<Ticket> ticketList = new ArrayList();

    @Autowired
    private IScrapeService scrapeService;

    @Autowired
    private ITicketService ticketService;

    @GetMapping("/scrape")
    public String scrape() {
        return "scrape";
    }

    /**
     * Submits scrape from the view
     *
     * @param site
     * @param keyword
     * @param proxy
     * @param hasImage
     * @param action
     * @param scheduleDate
     * @param fileType
     * @param userName
     * @param password
     * @return
     */
    @PostMapping(value = "/send")
    public @ResponseBody
    List sendRequest(String site, String keyword, String proxy,
                     String hasImage, String action, String scheduleDate,
                     String fileType, String userName, String password) {

        Progress progress = new Progress();

        log.info("Receving request from scrape");
        log.info("Site: " + site);
        log.info("Keyword: " + keyword);
        log.info("Proxy: " + proxy);
        log.info("Hasimage: " + hasImage);
        log.info("Action: " + action);
        log.info("Schedule: " + scheduleDate);

        if (keyword.isEmpty()) {
            keyword = "Apple laptop";
        }

        proxy = "191.122.122.2:8080";
        hasImage = "false";
        fileType = "xml";
        userName = "user";
        password = "password";

        Ticket ticket = new Ticket();
        ticket.setSiteId(1);
        ticket.setSiteName(site);
        ticket.setKeyword(keyword);
        ticket.setProxyAddress(proxy);
        ticket.setHasImage(Boolean.valueOf(hasImage));
        ticket.setAction(action);
        ticket.setSchedule(scheduleDate);
        ticket.setFileExport(fileType);
        ticket.setStatus("Active");
        ticket.setUserName("sample_username");
        ticket.setPassword("sample_password");
        ticket.setDateCreated(Date.valueOf(LocalDateTime.now().toLocalDate()));
        ticket.generateOtherDetails();

        ticketList.add(ticket);
        submit(ticket);

        return ticketList;
    }

    @GetMapping(path = "/progress")
    private @ResponseBody
    List getScrapeProgress() {
        log.info("Getting scrape progress");

        List progressList = new ArrayList();
        log.info("ticketList size: "+ticketList.size());
        for(Ticket ticket: ticketList){
            Progress progress = new Progress();

            progress.setTicketId(ticket.getTicketId());
            progress.setType(ticket.getType());
            progress.setSiteName(ticket.getSiteName());
            progress.setTicketStatus(ticket.getStatus());

            List<Scrape> scrapesFromTicket = ticket.getScrape();

            log.info("scrapeList size: "+scrapesFromTicket.size());

            for (Scrape scrape : scrapesFromTicket) {
                progress.setScrapeId(scrape.getScrapeId());
                progress.setScrapeStatus(scrape.getStatus());
                progress.setCurrentCount(scrape.getCurrentCount());
                progress.setTargetCount(scrape.getTargetCount());
                progress.setRemarks(scrape.getRemarks());
            }
            progressList.add(progress);
        }

        return progressList;
    }


    @PostMapping("addScrape")
    public ResponseEntity<Void> addScrape(@RequestBody Scrape scrape, UriComponentsBuilder builder) {
        boolean flag = scrapeService.addScrape(scrape);
        if (flag == false) {
            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(builder.path("/scrape/{id}").buildAndExpand(scrape.getScrapeId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

    @PostMapping("addTicket")
    public ResponseEntity<Void> addTicket(@RequestBody Ticket ticket, UriComponentsBuilder builder) {
        boolean flag = ticketService.addTicket(ticket);
        if (flag == false) {
            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(builder.path("/ticket/{id}").buildAndExpand(ticket.getTicketId()).toUri());
        return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
    }

    @GetMapping(path = "/getTickets")
    public ResponseEntity<List<Ticket>> getTicketsByStatus(String status) {
        List<Ticket> list = ticketService.getTicketsByStatus(status);
        return new ResponseEntity<List<Ticket>>(list, HttpStatus.OK);
    }

    /**
     * Submits ticket to the thread for execution
     * @param ticket
     */
    public void submit(Ticket ticket) {
        log.info("Submitting request " + ticket.getTicketId());

        ExecutorService executor = Executors.newFixedThreadPool(10);
        Set<Future<Scrape>> list = new HashSet<Future<Scrape>>();

        ScrapeThread callable = new ScrapeThread(ticket);
        Future<Scrape> future = executor.submit(callable);
        list.add(future);

        try {
            Scrape scrape = future.get();
            //     this.results = scrapeClone.getResults();
        } catch (InterruptedException e) {
            log.error("ScrapeRequest is interrupted!");
            e.printStackTrace();
        } catch (ExecutionException e) {
            log.error("Error executing scrape request!");
            e.printStackTrace();
        }
        executor.shutdown();
    }
}
