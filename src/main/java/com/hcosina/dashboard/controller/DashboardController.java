/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hcosina.dashboard.controller;

import com.hcosina.dashboard.util.FileUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 *
 * @author hcosina
 */
@Controller
public class DashboardController {
    @GetMapping("/home")
    public String records(){
        return "home";
    }
}
