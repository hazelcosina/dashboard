package com.hcosina.dashboard.repository;

import com.hcosina.dashboard.entity.Ticket;
import com.hcosina.dashboard.entity.TicketRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Repository
public class TicketDao implements ITicketDao{

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public TicketDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Ticket> getAllTickets() {
        String sql = "SELECT * FROM ticket";
        RowMapper<Ticket> rowMapper = new TicketRowMapper();
        return this.jdbcTemplate.query(sql, rowMapper);
    }

    @Override
    public List<Ticket> getTicketsByStatus(String status) {
        String sql = "SELECT * FROM ticket WHERE status = ? ";
        RowMapper<Ticket> rowMapper = new TicketRowMapper();
        return this.jdbcTemplate.query(sql, rowMapper, status);    }

    @Override
    public Ticket getTicketById(int ticketId) {
        String sql = "SELECT * FROM ticket WHERE ticket_id = ?";
        RowMapper<Ticket> rowMapper = new BeanPropertyRowMapper<Ticket>(Ticket.class);
        Ticket ticket = jdbcTemplate.queryForObject(sql, rowMapper, ticketId);
        return ticket;
    }

    @Override
    public void addTicket(Ticket ticket) {
        String sql = "INSERT INTO ticket (" +
                "site_id, "+
                "ticket_number, " +
                "keyword, " +
                "date_closed, " +
                "date_created, " +
                "status, " +
                "schedule, " +
                "action, " +
                "file_export, " +
                "has_image, " +
                "proxy_address, " +
                "user_name, " +
                "password, " +
                "is_on_repeat," +
                "site_name, " +
                "type) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        jdbcTemplate.update(sql,
                ticket.getSiteId(),
                ticket.getTicketNumber(),
                ticket.getKeyword(),
                ticket.getDateClosed(),
                ticket.getDateCreated(),
                ticket.getStatus(),
                ticket.getSchedule(),
                ticket.getAction(),
                ticket.getFileExport(),
                ticket.getHasImage(),
                ticket.getProxyAddress(),
                ticket.getUserName(),
                ticket.getPassword(),
                ticket.getIsOnRepeat(),
                ticket.getSiteName(),
                ticket.getType());

        sql = "SELECT ticket_id FROM ticket WHERE date_created= ? and ticket_number = ?";

        int ticketId = jdbcTemplate.queryForObject(sql, Integer.class, ticket.getDateCreated(), ticket.getTicketNumber());
        System.out.println("Ticket ID: "+ticketId);
        ticket.setTicketId(ticketId);
    }

    @Override
    public void updateTicket(Ticket ticket) {

    }

    @Override
    public void deleteTicket(int ticketId) {

    }

    @Override
    public boolean ticketExists(int ticketId) {
        return false;
    }
}
