package com.hcosina.dashboard.repository;

import com.hcosina.dashboard.entity.Scrape;

import java.util.List;

public interface IScrapeDao {
    List<Scrape> getAllScrapes();
    Scrape getScrapeById (int scrapeId);
    void addScrape(Scrape scrape);
    void updateScrape(Scrape scrape);
    void deleteScrape(int scrapeId);
    boolean scrapeExists(String scrapeId);
}
