package com.hcosina.dashboard.repository;

import com.hcosina.dashboard.entity.Site;

import java.util.List;

public interface ISiteDao {
    List<Site> getAllSites();
    Site getSiteById(int siteId);
    void addSite(Site site);
    void updateSite(Site site);
    void deleteSite(int siteId);
    boolean siteExists(String siteName);
}
