package com.hcosina.dashboard.repository;

import com.hcosina.dashboard.entity.Scrape;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Transactional
@Repository
public class ScrapeDao implements IScrapeDao{
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public ScrapeDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Scrape> getAllScrapes() {
        String sql = "SELECT ticket.ticket_id, scrape.* FROM scrape LEFT JOIN ticket ON ticket.ticket_id = scrape.ticket_id";
        List list = jdbcTemplate.queryForList(sql);
        return list;
    }

    @Override
    public Scrape getScrapeById(int scrapeId) {
        String sql = "SELECT * FROM scrape WHERE scrape_id = ?";
        RowMapper<Scrape> rowMapper = new BeanPropertyRowMapper<Scrape>(Scrape.class);
        Scrape scrape = jdbcTemplate.queryForObject(sql, rowMapper, scrapeId);
        return scrape;
    }

    @Override
    public void addScrape(Scrape scrape) {
        String retrieveLatest = "SELECT MAX(ticket_id) FROM ticket";
        int ticketId = jdbcTemplate.queryForObject(retrieveLatest, Integer.class);

        String sql = "INSERT INTO scrape (ticket_id, scrape_name, file_name, file_location, target_count, current_count, date_completed, date_started, status, remarks, job_trigger) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        jdbcTemplate.update(sql, ticketId, scrape.getScrapeName(), scrape.getFileName(), scrape.getFileLocation(), scrape.getTargetCount(), scrape.getCurrentCount(), scrape.getDateCompleted(), scrape.getDateStarted(), scrape.getStatus(), scrape.getRemarks(), scrape.getJobTrigger());
        sql = "SELECT scrape_id FROM scrape WHERE file_name= ?";

        int scrapeId = jdbcTemplate.queryForObject(sql, Integer.class, scrape.getFileName());
        scrape.setScrapeId(scrapeId);
    }

    @Override
    public void updateScrape(Scrape scrape) {

    }

    @Override
    public void deleteScrape(int scrapeId) {

    }

    @Override
    public boolean scrapeExists(String scrapeId) {
        return false;
    }
}
