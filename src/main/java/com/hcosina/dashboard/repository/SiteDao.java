package com.hcosina.dashboard.repository;

import com.hcosina.dashboard.entity.Site;
import com.hcosina.dashboard.entity.SiteRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Repository
public class SiteDao implements ISiteDao{
    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public SiteDao(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }


    @Override
    public List<Site> getAllSites() {
        String sql = "SELECT site_id, site_name FROM site";
        //RowMapper<Article> rowMapper = new BeanPropertyRowMapper<Article>(Article.class);
        RowMapper<Site> rowMapper = new SiteRowMapper();
        return this.jdbcTemplate.query(sql, rowMapper);
    }

    @Override
    public Site getSiteById(int siteId) {
        String sql = "SELECT site_id, site_name FROM site WHERE site_id = ?";
        RowMapper<Site> rowMapper = new BeanPropertyRowMapper<Site>(Site.class);
        Site article = jdbcTemplate.queryForObject(sql, rowMapper, siteId);
        return article;    }

    @Override
    public void addSite(Site site) {
        //Add site
        String sql = "INSERT INTO site (site_id, site_name) values (?, ?)";
        jdbcTemplate.update(sql, site.getSiteId(), site.getSiteName());

        //Fetch article id
        sql = "SELECT site_id FROM site WHERE site_name= ?";
        int articleId = jdbcTemplate.queryForObject(sql, Integer.class, site.getSiteName());

        //Set article id
        site.setSiteId(articleId);
    }

    @Override
    public void updateSite(Site site) {

    }

    @Override
    public void deleteSite(int siteId) {
        String sql = "DELETE FROM site WHERE siteId=?";
        jdbcTemplate.update(sql, siteId);
    }

    @Override
    public boolean siteExists(String siteName) {
        return false;
    }
}
