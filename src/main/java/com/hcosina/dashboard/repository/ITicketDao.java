package com.hcosina.dashboard.repository;

import com.hcosina.dashboard.entity.Ticket;
import org.eclipse.jetty.util.DateCache;

import java.util.List;

public interface ITicketDao {
    List<Ticket> getAllTickets();
    List<Ticket> getTicketsByStatus(String status);
    Ticket getTicketById (int ticketId);
    void addTicket(Ticket ticket);
    void updateTicket(Ticket ticket);
    void deleteTicket(int ticketId);
    boolean ticketExists(int ticketId);
}
