CREATE DATABASE  IF NOT EXISTS `dashboard`;
USE `dashboard`;

DROP TABLE IF EXISTS `User`;
CREATE TABLE `User` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(50) DEFAULT NULL,
  `lastName` varchar(50) DEFAULT NULL,
  `userRole` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

CREATE TABLE `Ticket` (
  `ticketId` int(11) unsigned **AUTO_INCREMENT**,
  `userId` int(11) unsigned DEFAULT NULL,
  `siteId` int(11) unsigned DEFAULT NULL,
  `ticketNumber` int(11) unsigned DEFAULT NULL,
  `dateCreated` datetime,
  `dateClosed` datetime,
  `status` varchar(20),
  `keyword` varchar(200),
  `schedule` varchar(20),
  `userName` varchar(20),
  `password` varchar(20),
  `schedule` varchar(20),
  `action` varchar(20),
  `fileExport` varchar(20),
  `hasImage` bool,
  `proxyAddress` varchar(50),
  PRIMARY KEY (`ticketId`),
  KEY `FK` (`userId`, `siteId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

CREATE TABLE `Site` (
  `siteId` int(11) unsigned **AUTO_INCREMENT**,
  `siteName` varchar(50),
  PRIMARY KEY (`siteId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

CREATE TABLE `Scrape` (
  `scrapeId` int(11) NOT NULL AUTO_INCREMENT,
  `ticketId` int(11) DEFAULT NULL,
  `scrapeName` varchar(100),
  `fileName` varchar(100),
  `fileLocation` varchar(200),
  `targetCount` int(50),
  `currentCount` int(50),
  `dateCompleted` varchar(50),
  `dateStarted` varchar(50),
  `status` varchar(50),
  `remarks` varchar(200),
  `jobTrigger` varchar(200),
  PRIMARY KEY (`scrapeId`),
  KEY `fk_ticket_id` (`ticketId`),
  CONSTRAINT `fk_ticket_id` FOREIGN KEY (`ticketId`) REFERENCES `ticket` (`ticketId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

CREATE TABLE `sites` (
	`siteId` INT(5) NOT NULL AUTO_INCREMENT,
	`siteName` VARCHAR(200) NOT NULL,
	PRIMARY KEY (`siteId`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
