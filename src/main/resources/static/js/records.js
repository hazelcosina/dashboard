
var currentIndex;
var itemsPerPage;
var currentPage;
var numberOfPages;
var numberOfRecords;
var numOfItemsToDelete;
var recordArr;

$(document).ready(function () {

    currentIndex = 0;
    itemsPerPage = 50;
    currentPage = 1;

    getRecords('All', 'All');

    $('#textfield-date').datepicker({dateFormat: 'yy/mm/dd'}).val();
    $('#textfield-date').on("change",function () {
        var date = $('#textfield-date').val();

        if (date !== '') {
            $('#select-site').removeClass('w3-disabled');
            $('#select-site').attr('disabled', false);
        }
    })
    $('#select-site').click(function () {
        var site = $('#select-site option:selected').text();
        if (site !== 'Select Site') {
            $('#button-search').removeClass('w3-disabled');
            $('#button-search').attr('disabled', false);
        } else {
          
        }
    })

    $('#next-page').click(function () {
        if (hasNextPage() === true) {
            $('#next-page').removeClass('w3-disabled');
            $('#next-page').attr('disabled',false);
        } else {
            $('#next-page').addClass('w3-disabled');
            $('#next-page').attr('disabled',true);
        }
        if (numberOfPages >= 2) {
            $('#prev-page').removeClass('w3-disabled');
        }
        $('#current-page').text(currentPage);

    })
    $('#prev-page').click(function () {
        if (hasPrevPage() === true) {
            $('#prev-page').removeClass('w3-disabled');
            $('#prev-page').attr('disabled',false);
        } else {
            $('#prev-page').addClass('w3-disabled');
            $('#prev-page').attr('disabled',true);
        }
        if (numberOfPages >= 2) {
            $('#next-page').removeClass('w3-disabled');
        }
        $('#current-page').text(currentPage);

    })

});

function getRecords(date, site) {
    
    recordArr = new Array();
    $.get("/getAllRecords", function (data) {
        var result = JSON.stringify(data);
        var resultArr = JSON.parse(result);
        recordArr = resultArr;
        populateRecordsTable(resultArr);
    })
    return recordArr;
}

function populateRecordsTable(resultArr) {
    
    numberOfRecords = resultArr.length;
    numOfItemsToDelete = itemsPerPage * 2;
    numberOfPages = getNumberOfPages();

    console.log('numberOfRecords: '+numberOfRecords);

    init();

    var index;
    for (index = 0; index < itemsPerPage; index++) {
        var eachResult = resultArr[currentIndex];

        if (eachResult) {
            var cnt = currentIndex + 1;
            var requestId = eachResult.scrape_id;
            var site = eachResult.scrape_name;
            var completed = eachResult.date_completed
            var ticketId = eachResult.ticket_id;
            var totalCount = eachResult.current_count +" / " +eachResult.target_count;
            var keyword = eachResult.keyword;
            var remarks = eachResult.remarks;


            var eachLine = '<tr>' +
                    '<td>' + cnt + '</td>' +
                    '<td>' + requestId + '</td>' +
                    '<td>' + ticketId + '</td>' +
                    '<td>' + site + '</td>' +
                    '<td>' + completed + '</td>' +
                    '<td>' + totalCount + '</td>' +
                     '<td>' + keyword + '</td>' +
                    '<td>' + remarks + '</td>' +
                    '<td><center>' +
                    '<span onclick="editRecord('+requestId+')" class="fa fa-external-link-square w3-xlarge w3-text-green"></span>  ' +
                    '<span id="delete-' + requestId + '"class="fa fa-close w3-xlarge w3-text-red"> </span> ' +
                    '</center></td>';
                    '</tr>';
            $('#tbl-records').append(eachLine);
            currentIndex++;
        } else {
            break;
        }
    }


    /* If items is lesser than itemsPerPage,
     * we subtract the extra slots and add it to currentIndex
     */
    if (index < itemsPerPage) {
        index = itemsPerPage - index;
        currentIndex = currentIndex + index;

    }
}

function editRecord(requestId){
    console.log('editRecord() requestId:'+requestId);
    window.open('view?scrapeid='+requestId);
}

function init() {
    var date = $('#textfield-date').val();
    var site = $('#select-site option:selected').text();

    $('#tbl-records').empty();

    if (date === '') {
        $('#select-site').addClass('w3-disabled');
        $('#select-site').attr('disabled', true);
    }
    if (site === 'Select Site') {
        $('#button-search').addClass('w3-disabled');
        $('#button-search').attr('disabled', true);
    }
    if(numberOfPages === 1){
        $('#next-page').addClass('w3-disabled');
        $('#next-page').attr('disabled');
    }
    
}

function getNumberOfPages() {

    var totalPages = parseInt(numberOfRecords / itemsPerPage);
    var remainder = numberOfRecords % itemsPerPage;

    if (remainder !== 0) {
        totalPages++;
    }

    return totalPages;
}

function hasNextPage() {
    if (currentPage >= numberOfPages) {
        return false;
    } else if (currentPage === numberOfPages - 1) {
        /* If last page is reached, add currentpage 
         * but disable next-button
         */
        currentPage++;
        populateRecordsTable(recordArr);

        return false;
    } else {
        currentPage++;
        populateRecordsTable(recordArr);
        return true;
    }
}

function hasPrevPage() {

    if (currentPage <= 1) {
        return false;
    } else if (currentPage === 2) {
        /* If first page is reached, subtract currentpage 
         * and disable prev-button
         */
        currentPage--;
        currentIndex = currentIndex - numOfItemsToDelete;
        populateRecordsTable(recordArr);

        return false;
    } else {
        currentPage--;
        currentIndex = currentIndex - numOfItemsToDelete;
        populateRecordsTable(recordArr);

        return true;
    }
}