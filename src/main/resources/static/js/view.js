

var idArr = new Array();
var keyArr = new Array();
var itemsArr = {};

$(document).ready(function () {

    var scrapeId = $('#scrapeid').text().replace('Record ','');
    getRecordDetail(scrapeId);

    $('#edit').click(function(){
        $('#table').addClass('w3-card-4 w3-margin w3-hoverable');
        $('#save').removeClass('w3-hide');
        $('#edit').addClass('w3-hide');

        //hiding print and download icons
        $('#print').addClass('w3-hide');
        $('#download').addClass('w3-hide');

        //showing sort icon
        $('#sort-price').removeClass('w3-hide');
        $('#sort-date').removeClass('w3-hide');
        $('#include-images').removeClass('w3-hide');
        $('#include-images-label').removeClass('w3-hide');

        //showing action column
        $('.action').removeClass('w3-hide');
    })

    $('#save').click(function(){
        $('#table').removeClass('w3-card-4 w3-margin w3-hoverable');
        $('#edit').removeClass('w3-hide');
        $('#save').addClass('w3-hide');

        //hiding print and download icons
        $('#print').removeClass('w3-hide');
        $('#download').removeClass('w3-hide');

        //hiding sorting tools
        $('#sort-price').addClass('w3-hide');
        $('#sort-date').addClass('w3-hide');
        $('#include-images').addClass('w3-hide');
        $('#include-images-label').addClass('w3-hide');

        //hiding action column
        $('.action').addClass('w3-hide');

        saveChanges();
    })

    $('#price').click(function(){
        sortPrice();
    })
    $('#download').click(function(){
        download();
    })

});

function getRecordDetail(id){
    console.log('Getting scrape detail for '+id);
     $.get("/getScrapeDetail?scrapeId="+id, function (data) {
            var result = JSON.stringify(data);
            var resultArr = JSON.parse(result);
            populateTable(resultArr);
            itemsArr = resultArr;
        })
}

function populateTable(resultArr){

    var data = resultArr[0];
    var keys = Object.keys(data);

    //Capitalizes first letter of a string
    String.prototype.capitalize = function() {
        return this.charAt(0).toUpperCase() + this.slice(1);
    }

    // Populating unique theads dynamically depending on the scrape details
     for(var x=0; x< keys.length; x++){
        var key = keys[x];

        // Condition is to exclude adding the default details ie title id images
        if(key != 'images' && key != 'title' && key != 'id'){
            var theadColumn = '<th id="'+key+'">'+key.capitalize()+'</th>';
            $('#thead-record tr').append(theadColumn);
            keyArr.push(key);
        }
      }

      var theadAction = '<th id="action" class="w3-hide action" style="width:10%"><center>Action</center></th>';
      $('#thead-record tr').append(theadAction);

    //Populate TRs on the default details
    for(var i=0; i<resultArr.length ; i++){

        var each = resultArr[i];

        // Default details: id, images, title
        var id = each.id;
        var images = each.images;
        var title = each.title;
        var imagesArr = images.split(',');
        var mainImage;

        if(imagesArr.length != null ){
            mainImage = imagesArr[0].replace('[','');
            mainImage = mainImage.replace(']','');
            //If there is no image
            if(mainImage == ''){
                mainImage = '/images/noimage.png';
            }
        }

        // Other details
        var otherDetails = '';
        for(var x=0; x< keys.length; x++){
            var key = keys[x];
            if(key != 'images' && key != 'title' && key != 'id'){
                var item = each[key];
                var details = '<td id="'+key+'" class="display"> ' + item + '</td>'+
                '<td id="edit-'+key+'" class="w3-hide editable"> <input type="text" class="w3-input" value="' + item + '"></td>';
                otherDetails = otherDetails + details;
            }
        }

         var actionDetailsRow = '<td class="w3-hide action">'+
            '<center><span class="fa fa-edit w3-xlarge w3-hoverable w3-hover-text-green" onclick="edit('+id+')"></span> &nbsp;' +
            '<span class="fa fa-check w3-xlarge w3-hoverable w3-hover-text-green w3-hide" onclick="saveRow('+id+')"></span> &nbsp;' +
            '<span class="fa fa-close w3-xlarge w3-text-red"></span></center></td>' +
            '</tr>';

        var commonDetailsRow =
            '<tr id="'+id+'"class="w3-animate-opacity">' +
            '<td> ' + id + '</td>' +
            '<td><img src="'+mainImage+'" style="width: 100%"></td>' +
            '<td id="title" class="display"> ' + title + ' </td>'+
            '<td id="edit-title" class="w3-hide editable"><input type="text" class="w3-input" value="' + title + '"></td>';
        $('#record').append(commonDetailsRow+ otherDetails + actionDetailsRow+'</tr>');

    idArr.push(id);
   }

}

function edit(id){

    $('#edit').click();
    $('#'+id+'  .editable').removeClass('w3-hide');
    $('#'+id+' .display').addClass('w3-hide');

    $('#'+id+' .fa-check').removeClass('w3-hide');
    $('#'+id+' .fa-edit').addClass('w3-hide');
}

function saveRow(id){

    //Display new values for Title
    var newTitleValue = $('#'+id+' #edit-title input').val();
    console.log('newValue: '+newTitleValue);
    $('#'+id+' #title').text(newTitleValue);

    //Save new values for Title
    itemsArr[id]['title'] = newTitleValue;

    //Display new values for other keys
    for(var x=0; x<keyArr.length; x++){
        var newKeyValue =  $('#'+id+' #edit-'+keyArr[x]+' input').val();
        $('#'+id+' #'+keyArr[x]+'').text(newKeyValue);
        itemsArr[id][keyArr[x]] = newKeyValue;
    }

    $('#'+id+'  .editable').addClass('w3-hide');
    $('#'+id+' .display').removeClass('w3-hide');

    $('#'+id+' .fa-check').addClass('w3-hide');
    $('#'+id+' .fa-edit').removeClass('w3-hide');


}

function sortPrice(){
    console.log('Sorting price.');

}

function download(){
    console.log('Downloading');
//    $.get("/download?newValueArr="+keyArr, function (data) {
//        console.log(data);
//    })
}

function saveChanges(){
console.log('Saving');

//    var excludedImagesArr = new Array();
//
//    for(var i=0 ; i<itemsArr.length; i++){
//        var eachItem = itemsArr[i];
//      //  var newItemsObj = {};
//        var newItemsObj;
//        var keys = Object.keys(eachItem);
//
//        for (var x=0 ; x< keys.length; x++){
//            var key = keys[x];
//            if(key !== 'images'){
//                //newItemsObj = {key: eachItem[key]};
//                newItemsObj = key+','+eachItem[key];
//            }
//       }
//
//       if(newItemsObj != null ){
//            excludedImagesArr.push(newItemsObj);
//       }
//    }

    $.post("/save?newvalues="+JSON.stringify(itemsArr), function (data) {
        console.log(data);
    })
}