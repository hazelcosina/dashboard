

var eachStatusRows = new Array();
$(document).ready(function () {

   // getUpdates();
    populateEachRowProgress();
    getTicketStatus("Active");

    $('#textfield-date').datepicker();
    $('.submit').click(function () {
        var site = $('#site-bar .w3-light-grey').text();
        submitScrape(site);
    });
    $('#radio-proxy-auto').click(function () {
        $('#auto-proxy').removeClass('w3-hide');
        $('#manual-proxy').addClass('w3-hide');
    });
    $('#radio-proxy-manual').click(function () {
        $('#manual-proxy').removeClass('w3-hide');
        $('#auto-proxy').addClass('w3-hide');
    });
    $('#radio-runondemand').click(function () {
        $('#repeat').addClass('w3-disabled');
        $('#date').addClass('w3-disabled');
    });
    $('#radio-setdate').click(function () {
        $('#repeat').addClass('w3-hide');
        $('#date').removeClass('w3-hide');
        $('#date').removeClass('w3-disabled');

        $('#select-day').val('invalid');
        $('#select-day-time').val('invalid');
    });
    $('#radio-repeat').click(function () {
        $('#repeat').removeClass('w3-hide');
        $('#repeat').removeClass('w3-disabled');

        $('#date').addClass('w3-hide');
        $('#textfield-date').val('DD/MM/YYYY');
        $('#select-date-time').val('invalid');
    });
});

function submitScrape(site) {
    $.notify("Successfully submitted!", "success");

    site = site.replace(' ','');
    var keyword = $('#textfield-keyword').val();
    var proxy = $('#textfield-proxy').val() + ":" + $('#textfield-port').val();
    var images = $('#checkbox-image').is(':checked');
    var action = $('input[type=radio][name=schedule]:checked').attr('id');
    var scheduleDate = $('#textfield-date').val();
    var scheduleDateTime = $('#select-date-time option:selected').val();
    var repeatDay = $('#select-day option:selected').val();
    var repeatDayTime = $('#select-day-time option:selected').val();
    var exportType = $('#input-export option:selected').text();
    var schedule = repeatDay + ',' + repeatDayTime + ',' + scheduleDate + ',' + scheduleDateTime;

    console.log('site: ' + site); //siteid
    console.log('keyword: ' + keyword); //data
    console.log('proxy: ' + proxy); //proxy
    console.log('image: ' + images); //boolean img
    console.log('run: ' + action);
    console.log('sched day: ' + repeatDay);
    console.log('sched daytime: ' + repeatDayTime);
    console.log('sched scheduleDate: ' + scheduleDate);
    console.log('sched scheduleDateTime: ' + scheduleDateTime);
    console.log('export type: ' + exportType);
    console.log('schedule date:' + scheduleDate);
    console.log(repeatDay + ',' + repeatDayTime + ',' + scheduleDate + ',' + scheduleDateTime);

    populateEachRowProgress();

    $.post("/send", {
        site: site,
        keyword: keyword,
        proxy: proxy,
        hasImage: images,
        action: action,
        scheduleDate: schedule,
        fileType: exportType}, function (data) {
    })
            .done(function () {
                $.notify("Done", "success");
            })
            .fail(function () {
                $.notify("Error Scraping", "error");
            })
            .always(function () {
                $('#input-keyword').val("");
                $('#input-proxy').val("");

            });
}

function getUpdates(){
    let updater = setInterval(() =>
        $.get("/getUpdates", function (data) {

            if(data === true){
                populateEachRowProgress();
            }
            })
           , 1000);
}

function populateEachRowProgress() {
    console.log("Populating progress rows");
    let timerId = setInterval(() =>
        $.get("/progress", function (data) {
            var result = JSON.stringify(data);
            var jsonResultArr = JSON.parse(result);
            if (jsonResultArr.length > 0) {
                hasRunningProgress(jsonResultArr);
            }
        }).done(function () {
            $('#loader-tbl-status').hide();
        }).fail(function(){
        $.notify('Failed populating each progress row');
        })
    , 1000);
}

function hasRunningProgress(jsonResultArr) {

    var eachProgress = new Array();
    var numberOfClosedTickets = 0;
    var numberOfActiveTickets = 0;
    var numberOfWarningTickets = 0;
    var numberOfRunningTickets = 0;

    for (var i = 0; i < jsonResultArr.length; i++) {

        var eachTicket = jsonResultArr[i];
        var site = eachTicket.siteName;
        var requestId = eachTicket.ticketId;
        var target = eachTicket.targetCount;
        var count = eachTicket.currentCount;
        var remarks = eachTicket.remarks;
        var type = eachTicket.type; //ano ito
        var scrapeStatus = eachTicket.scrapeStatus;
        var ticketStatus = eachTicket.ticketStatus;

        if(target == 0) {
            target = 100; //In case of an error in server
        }
        var progress = Math.round(100 / (target / count));

        if(scrapeStatus === 'Running'){
            numberOfRunningTickets++;
            if ($('#' + requestId).length === 0) {
               addTblStatusRow(requestId, site, type, count, target);
            }

            $('#' + requestId + ' #progress').css('width', +progress + '%');
            $('#' + requestId + ' #progress').text(progress + '%');
            $('#' + requestId + ' #progress-txt').text(remarks);
            $('#' + requestId + ' #count').text(count + ' / '+ target);
            $('#label-running-count').text(numberOfRunningTickets);

            eachProgress.push(progress);
        }

        if(scrapeStatus === 'Closed'){
            if ($('#' + requestId).length === 0) {
                addTblStatusRow(requestId, site, type, count, target);
            }
                 $('#' + requestId + ' #count').text(count + ' / '+ target);
                 if(progress >= 100){
                    progress = 100; //set default to 100
                    numberOfClosedTickets++;
                     $('#label-completed-count').text(numberOfClosedTickets);
                     $('#' + requestId + ' #progress').css('width', +progress + '%');
                     $('#' + requestId + ' #progress').text(progress + '%');
                     $('#' + requestId + ' #progress').attr('class','w3-container w3-blue');
                     $('#' + requestId + ' #progress-txt').text(remarks);
                 } else{
                     numberOfWarningTickets++;
                    $('#label-warning-count').text(numberOfWarningTickets);
                      $('#' + requestId + ' #progress').css('width', +progress + '%');
                      $('#' + requestId + ' #progress').text(progress + '%');
                      $('#' + requestId + ' #progress').attr('class','w3-container w3-orange');
                      $('#' + requestId + ' #progress-txt').text(remarks);
                 }
            }

        if(scrapeStatus === 'Failed'){
            numberOfFailedTickets ++;
            if ($('#' + requestId).length === 0) {
                addTblStatusRow(requestId, site, type);
            }
                $('#' + requestId + ' #progress').css('width', +progress + '%');
                $('#' + requestId + ' #progress').text(progress + '%');
                $('#' + requestId + ' #progress').attr('class','w3-container w3-red');
                $('#' + requestId + ' #progress-txt').text(remarks);
                $('#label-failed-count').text(numberOfFailedTickets);
        }
        if(ticketStatus === 'Active'){
            if ($('#tbl-tickets').has(requestId)) {
                 getTicketStatus('Active');
             }
        }

    }

    //Checks if there are running progress
    for (var i = 0; i < eachProgress.length; i++) {
        if (eachProgress[i] < 100) {
            return true;
        }
    }
    return false;
}

function addTblStatusRow(requestId, site, type, count, target) {

    $('#loader-tbl-status').show();

    if(type == 'scheduled_date'){
        type = 'Scheduled';
    }
    if(type == 'on_repeat'){
        type = 'On Repeat';
    }
    if(type == 'on_demand'){
        type = 'On Demand';
    }
    var eachStatusRow =
            '<tr id=' + requestId + '>' +
            '<td>' + requestId + '</td>' +
            '<td>' + site + '</td>' +
            '<td>' +
            '<div class="w3-white w3-animate-opacity">' +
            '<div id="progress" class="w3-container w3-green w3-animate-opacity" style="height:24px;width:0%">' +
            '</div>' +
            '</div>' +
            '<p id="progress-txt"> Starting </p>' +
            '</td>' +
            '<td>' + type + '</td>'+
            '<td id="count">' + count + ' / '+ target + '</td>'+
            '</tr>';

    eachStatusRows.push(eachStatusRow);
    $('#tbl-status').append(eachStatusRow);

    $('#loader-tbl-status').hide();
}


function getTicketStatus(status) {
    $.get("/getTickets", {status: status}, function (data) {
        var result = JSON.stringify(data);
        var resultArr = JSON.parse(result);

        $('#label-activetickets-count').text(resultArr.length);
        addActiveTicketsRow(resultArr);
    }).fail(function () {
        $.notify("Failed retrieving tickets", "error");
     })

}

function addActiveTicketsRow(resultArr){
       for (var i = 0; i < resultArr.length; i++) {

            var eachTicket = resultArr[i];
            var requestId = eachTicket.ticketId;
            var site = eachTicket.siteName;
            var keyword = eachTicket.keyword;
            var schedule = eachTicket.schedule;
            var action = eachTicket.action;
            var scheduleInWords = transformCronToWord(schedule, action);
            var tableActiveTicketsElement = $('#tbl-tickets').html();
            var requestIdElement = '<td> '+requestId+ ' </td>';

            if(tableActiveTicketsElement.indexOf(requestIdElement) === -1){
                var eachTicketsRow =
                        '<tr class="w3-animate-opacity">' +
                        '<td><i class="fa fa-calendar-check-o w3-text-brown w3-large"></i></td>' +
                        '<td> ' + requestId + ' </td>' +
                        '<td> ' + site + ' </td>' +
                        '<td> ' + keyword + ' </td>' +
                        '<td> ' + scheduleInWords + ' </td>' +
                        '<td><span class="fa fa-edit w3-large w3-hoverable w3-hover-text-green"></span> &nbsp;' +
                        '<span onclick="deleteTicket(' + requestId + ')" class="fa fa-close w3-large w3-text-red"></span></td>' +
                        '</tr>';
                $('#tbl-tickets').append(eachTicketsRow);
            }
        }
}

function deleteTicket(requestId) {

    $.get("/deleteTicket", {requestId: requestId}, function (data) {
        alert("Deleting");
    }).done(function () {
        alert("successfully deleted");
    })
}


function transformCronToWord(schedule, action) {
    var cron = schedule.split(' ');
    for (var i = 0; i < cron.length; i++) {

        if(action == "on_repeat"){
            var time = $('#select-day-time option[value='+cron[2]+']').text();
            var day = $('#select-day option[value='+cron[5]+']').text();
            return 'Every ' + day + ' at ' + time;
        }
        if(action == "scheduled_date"){
            var time = $('#select-date-time option[value='+cron[2]+']').text();

            var mm = cron[4];
            var dd = cron[3];
            var yyyy= cron[6];
            var completeDate = cron[4]+'/'+dd+'/'+yyyy;

            return 'On '+completeDate+' at '+time;
        }
        if(action == "on_demand"){
            var completeTime = cron[1].split(':');
            var hour = completeTime[0];
            var min = completeTime[1];
            var ampm;

            for(var i = 0, time = 12; i < 24; i++, time++){
                if(i >= 13){
                    ampm = "PM";
                } else{
                    ampm = "AM";
                }
                if(time >= 13){
                    time = 1;
                }
                if(i == hour){
                    return 'On '+cron[0]+ ' at '+time+':'+min+ ' '+ampm;
                }
            }
        }
    }
}