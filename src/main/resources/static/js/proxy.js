/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var currentProxyIndex;
var itemsPerPage;
var proxyArr;

$(document).ready(function () {

    currentProxyIndex = 0;
    itemsPerPage = 50;

    retrieveProxy('All', 'All');

    $('#select-proxy-location').click(function () {
        var country = $('#select-proxy-location option:selected').text();
        if (country !== 'Select Country') {
            $('#select-proxy-anonimity').removeClass('w3-disabled');
            $('#select-proxy-anonimity').removeAttr('disabled');
        } else {
            $('#select-proxy-anonimity').addClass('w3-disabled');
            $('#select-proxy-anonimity').attr('disabled', true);
        }
    })
    $('#select-proxy-anonimity').click(function () {
        var anon = $('#select-proxy-anonimity option:selected').text();
        if (anon !== 'Select Anonimity') {
            $('#button-filter-proxy').removeClass('w3-disabled');
            $('#button-filter-proxy').removeAttr('disabled');
        } else {
            $('#button-filter-proxy').addClass('w3-disabled');
            $('#button-filter-proxy').attr('disabled', true);
        }
    })
    $('#button-filter-proxy').click(function () {
        var country = $('#select-proxy-location option:selected').text();
        var anonimity = $('#select-proxy-anonimity option:selected').text();
        currentProxyIndex = 0;
        retrieveProxy(country, anonimity);

    })
    $('#button-refresh').click(function () {
        var country = $('#select-proxy-location option:selected').text();
        var anonimity = $('#select-proxy-anonimity option:selected').text();
        currentProxyIndex = 0;

        if (country !== 'Select Country' && anonimity !== 'Select Anonimity') {
            retrieveProxy(country, anonimity);
        } else{
            retrieveProxy('All','All');
        }
    })
});


function retrieveProxy(country, anonimity) {
    try {

        $('#tbl-proxy').empty();
        $.get('/retrieveproxy', {location: country, anonimity: anonimity}, function (data) {

            var result = JSON.stringify(data);
            var proxyArr = JSON.parse(result);

            init();

            var index;

            if (proxyArr.length !== 0) {
                $('#label-result').hide();
                for (index = 0; index < itemsPerPage; index++) {
                    var eachProxy = proxyArr[currentProxyIndex];

                    if (eachProxy) {

                        var cnt = currentProxyIndex + 1;
                        var location = eachProxy.location;
                        var address = eachProxy.proxyaddress;
                        var port = eachProxy.port;
                        var source = eachProxy.source;
                        var speed = eachProxy.speed;
                        var lastcheck = eachProxy.lastcheck;
                        var anonimity = eachProxy.anonimity;

                        addTblProxyRow(cnt, location, address, port, source, speed, lastcheck, anonimity);
                        currentProxyIndex++;

                    } else {
                        break;
                    }
                }
            } else {
                $('#label-result').show();
            }

        });
    } catch (e) {
        console.log('Exception in Proxy.js > retrieveProxy()');
    }
}

function addTblProxyRow(cnt, location, address, port, source, speed, lastcheck, anonimity) {

    if (anonimity === 'Elite') {
        anonimity = '<div class="w3-green">Elite</div>';
    } else if (anonimity === 'Transparent') {
        anonimity = '<div class="w3-orange">Transparent</div>';
    }

    var eachProxyRow =
            '<tr id=' + address + '>' +
            '<td>' + cnt + '</td>' +
            '<td>' + location + '</td>' +
            '<td>' + address + '</td>' +
            '<td>' + port + '</td>' +
            '<td>' + source + '</td>' +
            '<td>' + speed + '</td>' +
            '<td>' + lastcheck + '</td>' +
            '<td>' + anonimity + '</td>' +
            '<td><center><span value="' + address + ':' + port + '" class="fa fa-copy w3-large"></span></center></td>' +
            '</tr>';
    $('#tbl-proxy').append(eachProxyRow);
    return eachProxyRow;
}

function init() {
    var country = $('#select-proxy-location option:selected').text();
    var anon = $('#select-proxy-anonimity option:selected').text();

    if (country === 'Select Country') {
        $('#button-filter-proxy').addClass('w3-disabled');
        $('#button-filter-proxy').attr('disabled', true);
        $('#select-proxy-anonimity').addClass('w3-disabled');
        $('#select-proxy-anonimity').attr('disabled', true);
    } else {
        $('#button-filter-proxy').removeClass('w3-disabled');
        $('#button-filter-proxy').attr('disabled', false);
        $('#select-proxy-anonimity').removeClass('w3-disabled');
        $('#select-proxy-anonimity').attr('disabled', false);
    }
    if (anon === 'Select Anonimity') {
        $('#button-filter-proxy').addClass('w3-disabled');
        $('#button-filter-proxy').attr('disabled', true);
    } else {
        $('#button-filter-proxy').removeClass('w3-disabled');
        $('#button-filter-proxy').attr('disabled', false);
    }
}
