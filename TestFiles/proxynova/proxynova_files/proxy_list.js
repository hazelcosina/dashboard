
$(document).ready(function(){

	$("#proxy_country").change(function(){
	
		if(this.value != '')
		window.location = 'http://www.proxynova.com/proxy-server-list/country-'+this.value+'/';
		else window.location = 'http://www.proxynova.com/proxy-server-list/';
	
	}).click(function(){
		
		rememberPosition();
		
	});
	
	
	$("#lst_proxy_level").change(function(){
	
		if($(this).val() == 'all')
		{
			window.location = 'http://www.proxynova.com/proxy-server-list/';
			return;
		}
	
		window.location = 'http://www.proxynova.com/proxy-server-list/'+$(this).val()+'-proxies/';
		
	}).click(function(){
		rememberPosition();
	});
	
	
	loadPosition();

});


function loadPosition()
{
	var scroll_pos = $.cookie('scroll_pos');
	
	if(scroll_pos != null)
	{
		setTimeout(function(){
			$("html, body").scrollTop(scroll_pos);
		}, 1);
		
		$.cookie('scroll_pos', null);
	}
}

function rememberPosition()
{
	var from_top = $(window).scrollTop();
	var date = new Date(new Date().getTime()+(60 * 60 * 1000));
	
	$.cookie('scroll_pos', from_top);
}

function long2ip(ip) {
  //  discuss at: http://phpjs.org/functions/long2ip/
  // original by: Waldo Malqui Silva
  //   example 1: long2ip( 3221234342 );
  //   returns 1: '192.0.34.166'

  if (!isFinite(ip))
    return false;

  return [ip >>> 24, ip >>> 16 & 0xFF, ip >>> 8 & 0xFF, ip & 0xFF].join('.');
}
