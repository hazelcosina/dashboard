function include(css) {
        var styleElement = document.createElement('style');
        styleElement.setAttribute('type', 'text/css');
        styleElement.setAttribute('media', 'screen');
        if (styleElement.styleSheet) {
            styleElement.styleSheet.cssText = css;
        } else {
            styleElement.appendChild(document.createTextNode(css));
        }
        document.getElementsByTagName('head')[0].appendChild(styleElement);
    }
	
	


var css_main = "#feedback_icon {\
	background-position: 2px 50% !important;\
	position: fixed !important;\
	top: 45% !important;\
	display: block !important;\
	width: 25px !important;\
	height: 98px !important;\
	margin: -45px 0 0 0 !important;\
	padding: 0 !important;\
	z-index: 100001 !important;\
	background-repeat: no-repeat !important;\
	text-indent: -9000px;\
	left: 0;\
	background-repeat: no-repeat;\
	background-color:#509E4B;\
	background-image: url('https://www.php-proxy.com/feedback/feedback_tab_white.png');\
	border: 1px outset #509E4B;\
	border-left: none;\
	display:none;\
}\
#feedback_icon:hover {\
	background-color:#386F35;\
	cursor:pointer;\
}";



function feedback_init()
{
	var tab = document.createElement('a');
	tab.setAttribute('id', 'feedback_icon');
	//tab.innerHTML = html;
	
	tab.onclick = function(){

		var width = 400;
		var height = 400;
		var left = (screen.width/2)-(width/2);
		var top = (screen.height/2)-(height/2);

		window.open('https://www.php-proxy.com/feedback/feedback.php', 'newwindow', config='height='+height+', width='+width+', left='+left+', top='+top+', toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, directories=no, status=no');
	};
	
	document.body.insertBefore(tab, document.body.firstChild);

	include(css_main);
	
	console.log('feedback button loaded!');
}



if(window.attachEvent){
	window.attachEvent('onload', function(){
		feedback_init();
	});
}
else if(window.addEventListener)
{
	window.addEventListener('load', function(){
		
		feedback_init();
		
	}, false);
}